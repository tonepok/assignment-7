package no.ragnhildtone.WebAPI.controllers;

import no.ragnhildtone.WebAPI.models.Movie;
import no.ragnhildtone.WebAPI.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

//all the methods call the cossiponding service method where everything happens
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movies")
public class MovieController {
    @Autowired
    MovieService ms;

    //method to show all movies
    @GetMapping()
    public ResponseEntity<List<Movie>> getAllMovies() {
        return ms.getAllMovies();
    }

    //method to show a specific movie
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie(@PathVariable Long id) {
        return ms.getMovie(id);
    }

    //method to add a movie
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        return ms.addMovie(movie);
    }

    //method to update characters in a movie
    @PutMapping("/characters/{id}")
    public ResponseEntity<Movie> updateCharacters(@PathVariable Long id, @RequestBody List<Long> characters) {
        return ms.updateCharacters(id, characters);
    }

    //method to delete a movie
    @DeleteMapping("/{id}")
    public HttpStatus deleteMovie(@PathVariable Long id) {
        return ms.deleteMovie(id);
    }

    //method to get all characters in a movie
    @GetMapping("/{id}/characters")
    public ResponseEntity<List<Long>> getCharacters(@PathVariable Long id) {
        return ms.getCharacters(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie) {
        return ms.updateMovie(id, movie);
    }
}
