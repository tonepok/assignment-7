package no.ragnhildtone.WebAPI.controllers;

import no.ragnhildtone.WebAPI.models.Franchise;
import no.ragnhildtone.WebAPI.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

//all the methods call the corresponding service method where everything happens
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/franchises")
public class FranchiseController {
    @Autowired
    FranchiseService fs;

    //method to show all franchises
    @GetMapping()
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        return fs.getAllFranchises();
    }

    //method to show a specific franchise
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id){
        return fs.getFranchise(id);
    }

    //method to add a franchise
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        return fs.addFranchise(franchise);
    }

    //method to update a franchise
    @PutMapping("/movies/{id}")
    public ResponseEntity<Franchise> updateMovies(@PathVariable Long id, @RequestBody List<Long> moviesId){
        return fs.updateMovies(id, moviesId);
    }

    //method to delete a franchise
    @DeleteMapping("/{id}")
    public HttpStatus deleteFranchise(@PathVariable Long id){
        return fs.deleteFranchise(id);
    }

    //method to get all movies in a franchise
    @GetMapping("/{id}/movies")
    public ResponseEntity<List<Long>> getMovies(@PathVariable Long id){
        return fs.getMovies(id);
    }

    //method to get all characters in a franchise
    @GetMapping("/{id}/characters")
    public ResponseEntity<List<Long>> getCharacters(@PathVariable Long id){
        return fs.getCharacters(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise){
        return fs.updateFranchise(id, franchise);
    }
}
