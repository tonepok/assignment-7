package no.ragnhildtone.WebAPI.controllers;

import no.ragnhildtone.WebAPI.models.CharacterX;
import no.ragnhildtone.WebAPI.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

//all the methods call the corresponding service method where everything happens
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters")
public class CharacterController {
    @Autowired
    CharacterService cs;

    //method to show all characters
    @GetMapping()
    public ResponseEntity<List<CharacterX>> getAllCharacters(){
        return cs.getAllCharacters();
    }

    //method to show a specific character
    @GetMapping("/{id}")
    public ResponseEntity<CharacterX> getCharacter(@PathVariable Long id){
        return cs.getCharacter(id);
    }

    //method to add a character
    @PostMapping
    public ResponseEntity<CharacterX> addCharacter(@RequestBody CharacterX character){
        return cs.addCharacter(character);
    }

    //method to update a character
    @PutMapping("/{id}")
    public ResponseEntity<CharacterX> updateCharacter(@PathVariable Long id, @RequestBody CharacterX character){
        return cs.updateCharacter(id, character);
    }

    //method to delete a character
    @DeleteMapping("/{id}")
    public HttpStatus deleteCharacter(@PathVariable Long id){
        return cs.deleteCharacter(id);
    }

}
