package no.ragnhildtone.WebAPI.runners;

import no.ragnhildtone.WebAPI.models.CharacterX;
import no.ragnhildtone.WebAPI.models.Franchise;
import no.ragnhildtone.WebAPI.models.Movie;
import no.ragnhildtone.WebAPI.repositories.CharacterRepository;
import no.ragnhildtone.WebAPI.repositories.FranchiseRepository;
import no.ragnhildtone.WebAPI.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class AppStartupRunner implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(AppStartupRunner.class);

    @Autowired
    CharacterRepository characterRepository;
    @Autowired
    FranchiseRepository franchiseRepository;
    @Autowired
    MovieRepository movieRepository;

//     run method for development
//    @Override
//    public void run(ApplicationArguments args) throws Exception {
//        try {
//            ArrayList<CharacterX> charList = new ArrayList<>();
//            CharacterX gollum = new CharacterX("Gollum", "The First", "Smeagol", "male", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ20NGR3zH1Johrc3_3NFkgfGYaTGWFRjaXag&usqp=CAU");
//            CharacterX lightning = new CharacterX("Lightening", "McQueen", "Speedy", "male", "https://upload.wikimedia.org/wikipedia/en/8/82/Lightning_McQueen.png");
//            CharacterX frodo = new CharacterX("Frodo", "Baggins", "Shortie", "male", "https://1.bp.blogspot.com/--nN9E8LB86c/X7Ewn5GEGmI/AAAAAAAAGLc/ueRKDW39iUg_5uBBtUANVOv0jMsBufeIQCLcBGAsYHQ/s1280/Frodo%2BBaggins.png");
//            CharacterX sally = new CharacterX("Sally", "Carrera", "Cutie", "female", "https://static.wikia.nocookie.net/disney/images/2/27/Profile-_Sally_Carrera.png/revision/latest?cb=20190414094900");
//            charList.add(gollum);
//            charList.add(lightning);
//            charList.add(frodo);
//            charList.add(sally);
//            List<CharacterX> repChar = characterRepository.findAll();
//
//            ArrayList<Movie> movieList = new ArrayList<>();
//            Movie cars1 = new Movie("Cars", "Children", "2006", "John Lasseter", "https://www.imdb.com/title/tt0317219/mediaviewer/rm3794114560/", "https://www.youtube.com/watch?v=SbXIj2T-_uk");
//            Movie cars2 = new Movie("Cars 2", "Children", "2011", "John Lasseter", "https://www.imdb.com/title/tt1216475/mediaviewer/rm1951513344/", "https://www.youtube.com/watch?v=oFTfAdauCOo");
//            Movie lotr = new Movie("Lord of the Rings", "Fantasy", "2001", "Peter Jackson", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976/", "https://www.youtube.com/watch?v=_qVkcLUUrZQ");
//            movieList.add(cars1);
//            movieList.add(cars2);
//            movieList.add(lotr);
//            List<Movie> repMovie = movieRepository.findAll();
//
//            ArrayList<Franchise> franchisesList = new ArrayList<>();
//            Franchise carsF = new Franchise("Cars", "Cars universe");
//            Franchise lotrF = new Franchise("Lord of the Rings", "Amazing movies");
//            franchisesList.add(carsF);
//            franchisesList.add(lotrF);
//            List<Franchise> repFranch = franchiseRepository.findAll();
//
//
//            List<Movie> carsMovies = new ArrayList<>();
//            carsMovies.add(cars1);
//            carsMovies.add(cars2);
//            carsF.setMovies(carsMovies);
//            List<Movie> lotrMovies = new ArrayList<>();
//            lotrMovies.add(lotr);
//            lotrF.setMovies(lotrMovies);
//
//            List<CharacterX> carsChars = new ArrayList<>();
//            carsChars.add(lightning);
//            carsChars.add(sally);
//            cars1.setCharacters(carsChars);
//            cars2.setCharacters(carsChars);
//            List<CharacterX> lotrChars = new ArrayList<>();
//            lotrChars.add(frodo);
//            lotrChars.add(gollum);
//            lotr.setCharacters(lotrChars);
//
//            for (CharacterX cx : charList){
//                boolean found = false;
//                for (CharacterX rx : repChar){
//                    if (Objects.equals(cx.getPicture(), rx.getPicture())){
//                        found=true;
//                        break;
//                    }
//                }
//                if (!found){
//                    characterRepository.save(cx);
//                }
//            }
//            for (Movie mx : movieList){
//                boolean found = false;
//                for (Movie rx : repMovie){
//                    if (Objects.equals(mx.getPicture(), rx.getPicture())){
//                        found=true;
//                        break;
//                    }
//                }
//                if (!found){
//                    movieRepository.save(mx);
//                }
//            }
//            for (Franchise fx : franchisesList){
//                boolean found = false;
//                for (Franchise rx : repFranch){
//                    if (Objects.equals(fx.getName(), rx.getName())){
//                        found=true;
//                        break;
//                    }
//                }
//                if (!found){
//                    franchiseRepository.save(fx);
//                }
//            }
//
//        }
//        catch (Exception e){
//            System.out.println(e.getMessage());
//        }
//    }

    // run method for production
    @Override
    public void run(ApplicationArguments args) throws Exception {
        try {
            if(
                    movieRepository.count() == 0 &&
                            characterRepository.count() == 0 &&
                            franchiseRepository.count() == 0
            ) {
                ArrayList<CharacterX> charList = new ArrayList<>();
                CharacterX gollum = new CharacterX("Gollum", "The First", "Smeagol", "male", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ20NGR3zH1Johrc3_3NFkgfGYaTGWFRjaXag&usqp=CAU");
                CharacterX lightning = new CharacterX("Lightening", "McQueen", "Speedy", "male", "https://upload.wikimedia.org/wikipedia/en/8/82/Lightning_McQueen.png");
                CharacterX frodo = new CharacterX("Frodo", "Baggins", "Shortie", "male", "https://1.bp.blogspot.com/--nN9E8LB86c/X7Ewn5GEGmI/AAAAAAAAGLc/ueRKDW39iUg_5uBBtUANVOv0jMsBufeIQCLcBGAsYHQ/s1280/Frodo%2BBaggins.png");
                CharacterX sally = new CharacterX("Sally", "Carrera", "Cutie", "female", "https://static.wikia.nocookie.net/disney/images/2/27/Profile-_Sally_Carrera.png/revision/latest?cb=20190414094900");
                charList.add(gollum);
                charList.add(lightning);
                charList.add(frodo);
                charList.add(sally);
                List<CharacterX> repChar = characterRepository.findAll();

                ArrayList<Movie> movieList = new ArrayList<>();
                Movie cars1 = new Movie("Cars", "Children", "2006", "John Lasseter", "https://www.imdb.com/title/tt0317219/mediaviewer/rm3794114560/", "https://www.youtube.com/watch?v=SbXIj2T-_uk");
                Movie cars2 = new Movie("Cars 2", "Children", "2011", "John Lasseter", "https://www.imdb.com/title/tt1216475/mediaviewer/rm1951513344/", "https://www.youtube.com/watch?v=oFTfAdauCOo");
                Movie lotr = new Movie("Lord of the Rings", "Fantasy", "2001", "Peter Jackson", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976/", "https://www.youtube.com/watch?v=_qVkcLUUrZQ");
                movieList.add(cars1);
                movieList.add(cars2);
                movieList.add(lotr);
                List<Movie> repMovie = movieRepository.findAll();

                ArrayList<Franchise> franchisesList = new ArrayList<>();
                Franchise carsF = new Franchise("Cars", "Cars universe");
                Franchise lotrF = new Franchise("Lord of the Rings", "Amazing movies");
                franchisesList.add(carsF);
                franchisesList.add(lotrF);
                List<Franchise> repFranch = franchiseRepository.findAll();


                List<Movie> carsMovies = new ArrayList<>();
                carsMovies.add(cars1);
                carsMovies.add(cars2);
                carsF.setMovies(carsMovies);
                List<Movie> lotrMovies = new ArrayList<>();
                lotrMovies.add(lotr);
                lotrF.setMovies(lotrMovies);

                List<CharacterX> carsChars = new ArrayList<>();
                carsChars.add(lightning);
                carsChars.add(sally);
                cars1.setCharacters(carsChars);
                cars2.setCharacters(carsChars);
                List<CharacterX> lotrChars = new ArrayList<>();
                lotrChars.add(frodo);
                lotrChars.add(gollum);
                lotr.setCharacters(lotrChars);

                for (CharacterX cx : charList){
                    characterRepository.save(cx);
                }
                for (Movie mx : movieList){
                    movieRepository.save(mx);
                }
                for (Franchise fx : franchisesList){
                    franchiseRepository.save(fx);
                }
            }

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}