package no.ragnhildtone.WebAPI.repositories;

import no.ragnhildtone.WebAPI.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {

}
