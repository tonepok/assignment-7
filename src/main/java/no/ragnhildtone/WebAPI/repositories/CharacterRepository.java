package no.ragnhildtone.WebAPI.repositories;

import no.ragnhildtone.WebAPI.models.CharacterX;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<CharacterX, Long> {

}
