# Assignment 7
Spring Boot application in Java. The application is about creating a Postgres database with Hibernate and show it with a Web API.

### Maintainers
- Tone Eggan Pokorny: @tonepok
- Ragnhild Emblem Holte: @reholte
- Julie Solvin Jacobsen: @juliesol

### Program description
Program that creates a Web API and database, and seeds it. Then you can apply CRUD on each model. 

### Install
- In terminal: `git clone https://gitlab.com/tonepok/assignment-7.git`
- Open the folder in Intellij

### How to run
Run by right-clicking WebApiApplication and select Run 'WebApiApplication.main()'

### Notes
**AppStartupRunner:**  
Two run()-methods: One for production, one for development. Comment out the method you don't want to use, and uncomment the method you want to use.
